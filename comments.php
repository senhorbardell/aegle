<?php
/**
 * Comments template.
 * 
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.1
 */
if ( have_comments() ) : ?>
	<div class="comments">
		<ul>
			<?php wp_list_comments('callback=metro_comments'); ?>
		</ul>
	</div>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
	<nav id="comment-nav-below">

		<h1 class="assistive-text">Navigation</h1>

		<div class="nav-previous">
			<?php previous_comments_link( '&larr; Older Comments'); ?>
		</div>

		<div class="nav-next">
			<?php next_comments_link('Newer Comments &rarr;'); ?>
		</div>

	</nav>
	<?php endif; // check for comment navigation ?>

<?php elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
	
<?php endif; ?>

<?php $args = array(
		'comment_notes_after'=>'',
		'logged_in_as'=>'<p class="logged-in-as">Oh, <a href="'.admin_url( 'profile.php' ).'">'.$user_identity.'</a> is it you? <a title="Logout" href="'.wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) ).'">Nope</a></p>',
		'comment_notes_before'=>'',
		'comment_field'=>'<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
		'title_reply'=>'Leave a reply'
	); ?>

<?php comment_form($args); ?>
