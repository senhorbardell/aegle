# Require any additional compass plugins here.

enviroment = :development

http_path = "localhost/wordpress"
css_dir = "css/stylesheets"
sass_dir = "css/sass"
images_dir = "img"
javascripts_dir = "js"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

if enviroment == :production
	output_style = :compressed
end

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

preferred_syntax = :sass
