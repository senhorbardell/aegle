<?php 
/**
 * Template for displaying single page.
 *
 * @package Wordpress
 * @subpackage Aegle
 * @since Aegle 1.1
 */
get_header(); ?>

<section class="content">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if (!is_front_page()) : ?>
			<h1 class="title">
				<a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
			</h1>
		<?php endif ?>

		<?php if ( has_post_thumbnail() ) {
			the_post_thumbnail();
		} ?>

		<?php while ( have_posts() ) : the_post() ?>

			<?php the_content() ?>

	</article>

	<?php comments_template() ?>

</section>

		<?php endwhile ?>

	<?php wp_link_pages(); ?>

<?php get_footer(); ?>