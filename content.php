<?php
/**
 * Page partial, responsible for category and front page.
 *
 * @package Wordpress
 * @subpackage Aegle
 * @since Aegle 1.1
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<header>
		<h1 class="title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h1>
	</header>

	<?php if ( has_post_thumbnail() ) : ?>
		<figure>
			<?php the_post_thumbnail('galley-thumbnails') ?>
		</figure>
	<?php endif ?>

		<section class="content">
			<?php the_content(false) ?>
		</section>

		<footer>
			<time  
				datetime="<?php echo get_the_date('c') ?>" 
				pubdate>
					<?php echo get_the_date() ?>
				</time>

			<a href="<?php the_permalink() ?>" class="more">Read more</a>
		</footer>

</article>