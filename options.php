<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses 'options_theme_customizer', can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.1
 */

function optionsframework_option_name() {

    // This gets the theme name from the stylesheet
    $themename = get_option( 'stylesheet' );
    $themename = preg_replace("/\W/", "_", strtolower($themename) );

    $optionsframework_settings = get_option( 'optionsframework' );
    $optionsframework_settings['id'] = 'options_theme_customizer';
    /**
     * I'm explicitly setting options settings ID
     */
    update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 *
 * Notice $options['id'], it for customizer, also by doing this you can you standard get_option()
 */

function optionsframework_options() {

    $options = array();

    $options[] = array(
        'name' => 'Settings', 'options_framework_theme',
        'type' => 'heading');

    $options['footer_text'] = array(
            'name' => 'Footer text',
            'desc' => 'information, copyright, etc...',
            'id' => 'footer_text',
            'std' => 'Proudly maded by <a href="http://senhorbardell.com">John Bardell</a> with <a href="http://wordpress.org">Wordpress</a>.',
            'type' => 'textarea');

    $options['widgets_in_front'] = array(
        'name' => 'Widgets',
        'desc' => 'Allow widgets to be on front page',
        'id' => 'widgets_in_front',
        'std' => '0',
        'type' => 'checkbox');

    $options['quick_menu'] = array(
        'name' => 'Quick menu',
        'desc' => 'Hide quick menu',
        'id' => 'quick_menu',
        'std' => '0',
        'type' => 'checkbox');

    $options['enable_logo'] = array(
        'name' => 'Logo',
        'desc' => 'Enable logo',
        'id' => 'enable_logo',
        'std' => '0',
        'type' => 'checkbox');

    $options['logo_url'] = array(
        'name' => 'Pick a logo',
        'desc' => 'Medium size',
        'id' => 'logo_uploader',
        'type' => 'upload');

    $colors_array = array(
        'style.min.css' => 'Blue (default)',
        'style-green.min.css' => 'Green',
        'style-violet.min.css' => 'Violet',
        'style-purple.min.css' => 'Purple',
        'style-dark-purple.min.css' => 'Dark Purple',
        'style-teal.min.css' => 'Teal',
        'style-red.min.css' => 'Red',
        'style-orange.min.css' => 'Orange',
        'style-sky-blue.min.css' => 'Sky Blue',
    );

    $expanded_colors_array = array(
        'style.css' => 'Blue (default)',
        'style-green.css' => 'Green',
        'style-violet.css' => 'Violet',
        'style-purple.css' => 'Violet',
        'style-teal.css' => 'Violet',
        'style-red.css' => 'Violet',
        'style-orange.css' => 'Violet',
        'style-sky-blue.css' => 'Violet',
    );

    $options[] = array(
        'name' => 'Theme feel',
        'desc' => 'Colors',
        'id' => 'style',
        'std' => 'one',
        'type' => 'select',
        'class' => 'mini',
        'options' => $colors_array);


    $options[] = array(
        'name' => 'Contact info', 'options_framework_theme',
        'type' => 'heading');

    $options[] = array(
            'name' => 'Would really like to hear your feedback.',
            'desc' => '<ul><li><a href="mailto:senhorbardell@gmail.com">senhorbardell@gmail.com</a></li><li><a href="https://twitter.com/SenhorBardell">@senhorbardell</a></li><li><a href="http://themeforest.net/user/SenhorBardell">Themeforest</a></li><ul>',
            'type' => 'info'
        );


    return $options;
}

/**
 * Wordpress' new customizer feature.
 */

function options_theme_customizer_register($wp_customize) {
    $options = optionsframework_options();

    /**
     * Sections
     */
    
    $wp_customize->add_section('options_theme_customizer_basic', array(
            'title' => 'Basic',
            'priority' => 30
        ));

    /**
     * Settings
     */
    
    $wp_customize->add_setting( 'options_theme_customizer[footer_text]', array(
        'default' => $options['footer_text']['std'],
        'type' => 'option'
    ) );

    $wp_customize->add_setting('options_theme_customizer[widgets_in_front]', array(
            'default' => $options['widgets_in_front']['std'],
            'type' => 'option'
        ));

    $wp_customize->add_setting('options_theme_customizer[quick_menu]', array(
        'default' => $options['quick_menu']['std'],
        'type' => 'option'
    ));

    $wp_customize->add_setting('options_theme_customizer[enable_logo]', array(
        'default' => $options['enable_logo']['std'],
        'type' => 'option'
    ));

    /**
     * Controls
     */
    
    $wp_customize->add_control( 'options_theme_customizer_footer_text', array(
        'label' => $options['footer_text']['name'],
        'section' => 'options_theme_customizer_basic',
        'settings' => 'options_theme_customizer[footer_text]',
        'type' => 'text'
    ) );

    $wp_customize->add_control('options_theme_customizer_widgets_in_front', array(
            'label' => $options['widgets_in_front']['desc'],
            'section' => 'options_theme_customizer_basic',
            'settings' => 'options_theme_customizer[widgets_in_front]',
            'type' => $options['widgets_in_front']['type']
        ));

    $wp_customize->add_control('options_theme_customizer_quick_menu', array(
        'label' => $options['quick_menu']['desc'],
        'section' => 'options_theme_customizer_basic',
        'settings' => 'options_theme_customizer[quick_menu]',
        'type' => $options['quick_menu']['type']
    ));

    $wp_customize->add_control('options_theme_customizer_logo', array(
        'label' => $options['enable_logo']['desc'],
        'section' => 'options_theme_customizer_basic',
        'settings' => 'options_theme_customizer[enable_logo]',
        'type' => $options['enable_logo']['type']
    ));

}

add_action('customize_register', 'options_theme_customizer_register');