<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.1
 */
?>

<footer>
	<div id="ribbon"></div>

	<?php if(!is_front_page() && !of_get_option('quick_menu')): ?>

		<div class="quick-nav">
			<ul>
				<li><a href="#" title="Top">&#x24;</a></li>

			<?php if(comments_open()) :?>
				<li><a href="#reply-title" title="Reply">&#x21;</a></li>
			<?php endif ?>

				<?php if (is_category()) : ?>
					<li><?php previous_posts_link('&#x26;') ?></li>
					<li><?php next_posts_link('&#x23;') ?></li>
				<?php else : ?>
					<li><?php previous_post_link('%link', '&#x26;') ?></li>
					<li><?php next_post_link('%link', '&#x23;') ?></li>
				<?php endif ?>
			</ul>
		</div>
	
	<?php endif ?>


	<?php if(of_get_option('widgets_in_front', false) && is_active_sidebar( 'sidebar-1' ) || !is_front_page() && is_active_sidebar( 'sidebar-1' )): ?>

		<section class="footer-widgets">
			<div class="inner">
				<?php dynamic_sidebar() ?>
			</div>
		</section>
		
	<?php endif ?>

	<?php 
	/* Started to regret this decision
	 * This is the main menu
	 * This menu will be on TOP, i made it because due to mobile 
	 * expierence, its better to have menu at the bottom,
	 * right below article
	 */ ?>
	
	<?php get_search_form($echo = true) ?>
	
	<?php wp_nav_menu(array(
		'theme_location' => 'main_menu',
		'container' => '',
		'container_id' => 'menu',
		'menu_class' => 'menu',
		'menu_id' => '',
		)) ?>

	<div class="copyright">
			<p>
				<?php echo of_get_option('footer_text', 'Proudly maded by <a href="http://senhorbardell.com">John Bardell</a> with <a href="http://wordpress.org">Wordpress</a>.') ?>
			</p>
	</div>

</footer>

<?php wp_footer() ?>

</body>
</html>