<?php
/**
 * The template for displaying search results.
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.0
 */
?>

<?php get_header() ?>

	<section class="content">

	<?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : the_post() ?>
			
				<?php get_template_part( 'content' ) ?>
			
		<?php endwhile ?>

		
	<?php else: ?>

			<article>
				
				<header>
					<h1 class="title">Not found</h1>
				</header>
				<section class="content">
					<p>
						Seems like we can't find what you looking for. Try searching again.
					</p>
				</section>

			</article>


	<?php endif ?>

	</section>

<?php get_footer() ?> 