<?php 
/**
 * Template Name: Always with logo
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.1
 */
get_header(); ?>
<div class="logo">
			<img src="<?php echo of_get_option('logo_uploader', '') ?>" >
</div>
<section class="content" >

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if (!is_front_page()) : ?>
			<h1 class="title">
				<a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
			</h1>
		<?php endif ?>

		<?php while ( have_posts() ) : the_post() ?>

			<?php the_content() ?>

	</article>

	<?php comments_template() ?>

</section>

		<?php endwhile ?>

<?php get_footer(); ?>