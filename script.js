(function($) {
	$.fn.baseline = function(breakpoints) {

		// Set up our variables, like a good little developer
		var tall, newHeight, base, old = 0;

		return this.each(function(){
			var $this = $(this); // Set the images as objects

			var setbase = function(breakpoints) { // The fun starts here

				// Check if a single value or multiple breakpoints are given
                if (typeof breakpoints === 'number') {
                    base = breakpoints;
                } else if (typeof breakpoints === 'object') {
                    // Loop through the breakpoints and check which baseline to apply
                    for (var key in breakpoints) {
                        var current = parseInt(key, 10);
                        if (document.width > current && current >= old) {
                            base = breakpoints[key];
                            old = current;
                        }
                    }
                }

				$this.css('maxHeight', 'none'); // Remove old max-height so that we can resize up as well as down
				// $this.height('');
				tall = $this.height(); // Grab the height
				newHeight = Math.floor(tall / base) * base; // Make up a new height based on the baseline
				$this.parent().height(newHeight); // Set it!
				if ($this.parent().parent('figure')) {
					$this.parent().parent('figure').height(newHeight);
				}
				// $this.height(newHeight); // Set it!
				$this.css('maxHeight', newHeight);
				// console.log('resize images to ' + $this.height());
			};

			setbase(breakpoints); // Call on load

			$(window).resize(function(){ // And call again on resize.
				setbase(breakpoints);
			});

		});

	};

	var on_off, intervalListener,
	$ribbon = $('#ribbon'),
	$menuMain = $('#menu-main-menu'),
	$mobileMenu = $('#mobile-menu'),
	$mobileSearch = $('#mobile-search'),
	$search = $('.searchform'),
	$quickMenu = $('.quick-nav'),
	$window = $(window), ribbon_is_up, win_width, opacity_timeout = null;


	var middle = 600, wide = 1000;
	win_width = $window.width();

	reinitialize_binds(win_width);

	/*
		Ribbon functions.
	 */

	/**
	 * Hover functiion
	 */
	function ribbon_up() {
		if (!ribbon_is_up ) {
			$ribbon.addClass('ribbon-pull');
			$menuMain.addClass('menu-main-pull');
			clearInterval(intervalListener);
			console.log('up');
			ribbon_is_up = true;
		} else {
			console.log('ribbon is already up');
		}
	}

	function ribbon_down() {
		$ribbon.removeClass('ribbon-pull');
		$menuMain.removeClass('menu-main-pull');
		clearInterval(intervalListener);
		console.log('down');
		ribbon_is_up = false;
	}

	/**
	 * Leave function
	 */
	function ribbon_mouseleave() {
		if (ribbon_up) {
			intervalListener = setInterval(ribbon_down, 3000);
		}
	}

	/*
		Main menu functions
	 */
	
	function mobileMenuClick() {
		$(window).scrollTop($menuMain.position().top);
	}

	function searchMenuClick() {
		$search.toggleClass('hide');
	}

	function menuMain_mouseenter() {
		clearInterval(intervalListener);
		console.log('waiting...');
		console.log(intervalListener);
	}

	function menuMain_mouseleave() {
		if (ribbon_up) {
			intervalListener = setInterval(ribbon_down, 3000);
		}
	}

	function menu_up() {
		$menuMain.toggleClass('show');
		$(window).scrollTop($menuMain.position().top);
	}

	function menu_down() {
		$menuMain.toggleClass('show');
	}

	/*
		Quick navigation functions
	 */
	function quicknav_up() {
		$quickMenu.css('opacity', '1');
		console.log('up');
		clearInterval(opacity_timeout);
		opacity_timeout = setInterval(quicknav_down, 1000);
	}

	function quicknav_enter() {
		$quickMenu.css('opacity', '1');
		clearInterval(opacity_timeout);
	}

	function quicknav_down() {
		console.log('down');
		$quickMenu.css('opacity', '0');
		clearInterval(opacity_timeout);
	}

	/**
	 * This function will listen subscribe and unsubscribe to listeners
	 * regarding device width.
	 * @todo need mobile testing
	 * @param  {number} width window width
	 */

	function reinitialize_binds(width) {
		console.log(width);

		if (width >= middle && width < wide) {
			// Medium

			$menuMain.removeClass('hide');
			$search.removeClass('hide');
			$menuMain.removeClass('menu-main-pull');
			$ribbon.removeClass('ribbon-pull');


			$ribbon.bind('mouseenter', ribbon_up);
			$ribbon.bind('click', ribbon_up);
			$ribbon.bind('mouseleave', ribbon_mouseleave);

			ribbon_down();

			$menuMain.bind('mouseenter', menuMain_mouseenter);
			$menuMain.bind('mouseleave', menuMain_mouseleave);

			$window.unbind('scroll');

			$quickMenu.unbind('mouseenter');
			$quickMenu.unbind('mouseleave');

			console.log('middle');

		} else if (width > wide) {
			// Wide

			$menuMain.removeClass('hide');
			$search.removeClass('hide');
			$menuMain.removeClass('menu-main-pull');
			$ribbon.removeClass('ribbon-pull');
			$quickMenu.css('opacity', '0');

			$ribbon.unbind('mouseenter');
			$ribbon.unbind('mouseleave');

			clearInterval(intervalListener);

			$window.bind('scroll', quicknav_up);

		$menuMain.unbind('mouseenter');
			$menuMain.unbind('mouseleave');

			$quickMenu.bind('mouseenter', quicknav_enter);
			$quickMenu.bind('mouseleave', quicknav_down);

			console.log('wide');

		} else if (width < middle) { // if 343 < 600
			// Mobile

			$menuMain.removeClass('hide');
			$search.removeClass('hide');
			$menuMain.removeClass('menu-main-pull');
			$ribbon.removeClass('ribbon-pull');

			$ribbon.unbind('mouseenter');
			$ribbon.unbind('mouseleave');

			$menuMain.unbind('mouseenter');
			$menuMain.unbind('mouseleave');

			$window.unbind('scroll');


			$quickMenu.unbind('mouseenter');
			$quickMenu.unbind('mouseleave');

			$mobileMenu.bind('click', mobileMenuClick);
			// $mobileSearch.bind('click', searchMenuClick);
			
			/**
			 *	Sub-menu functionality
			 *	@todo middle-width, wide-width.
			 */
			
			$('.menu-item a').click(function(e) {
				var $sub_menu = $(this).next('.sub-menu');
				if ($sub_menu.length >= 1) {
					e.preventDefault();
					$sub_menu.toggleClass('sub-menu-show');
					$(this).toggleClass('selected');
				} else {
					console.log('no sub-menus');
				}
			});


			console.log('mobile');
		}

	}

	/*
	 * Purely aesthetic function, useless, unless you resize a lot.
	 */
	$window.resize(function() {
		var new_width = $(window).width();
		console.log('new width: ' + new_width);
		reinitialize_binds(new_width);
	});

	/**
	 * Baseline initialization
	 */

	$('article img, .logo img, .attachment img').load(function() {
		$(this).baseline({'0px':21, '600px':24, '1000px':27});
	});

	$('iframe').load(function() {
		$(this).baseline({'0px':21, '600px':24, '1000px':27});
	});

}) (jQuery);