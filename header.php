<?php
/**
 * The Header for our theme.
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.0
 */
?>

<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<title>
		<?php is_front_page() || is_category() ? bloginfo('name') : the_title() ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php wp_head() ?>

</head>
<body <?php body_class() ?>>
	<header>
		<?php # main  menu in the footer, trolls... ?>
		<a id="mobile-search" href="#search">&#x27;</a>
		<a id="mobile-menu" href="#menu">&#x28;</a>
		<?php if (comments_open() && !is_front_page()) : ?>
			<a id="comments" href="#respond">&#x21;</a>
		<?php endif ?>
		<?php if (!is_front_page()): ?>
			<?php previous_post_link('%link', '&#x26;') ?>
			<?php next_post_link('%link', '&#x23;') ?>
		<?php endif ?>
	</header>
	<?php if(of_get_option('enable_logo') && is_front_page()): ?>
		<div class="logo">
			<img src="<?php echo of_get_option('logo_uploader', '') ?>" >
		</div>
	<?php endif ?>