<?php
/**
 * Metropolis function and definitions.
 *
 * @package  Wordpress
 * @subpackage Aegle
 * @since  Aegle 1.1
 */


add_action('init', 'load');

function load() {
	if (!is_admin()) {
        // $style = '/css/stylesheets/' . of_get_option('style', 'style.min.css');
        $style = '/css/stylesheets/style.css';
		wp_enqueue_style('main', get_template_directory_uri().$style);
		wp_deregister_script('jquery');
		wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '');
		wp_enqueue_script('main', get_template_directory_uri().'/script.min.js', array('jquery'), false, true);
	}
}

// Menu

add_action('init', 'register_my_menu');

function register_my_menu() {
	register_nav_menus(array('main_menu' => 'Menu'));
}

add_action('init', 'register_my_sidebar');

function register_my_sidebar() {
	register_sidebar(array(
		'before_widget' => '<section class="widget">', 
		'after_widget' => '</section>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}

if ( ! isset( $content_width ) ) $content_width = 630;

if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
	require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}


$t_path = get_template_directory();

require( $t_path . '/options.php' );

require( $t_path . '/comments_functions.php');

include_once( $t_path . '/shortcodes.php');

/**
 * Customized wordpress excerpt.
 * 
 * @param string $text
 */
function improved_trim_excerpt($text) {
	global $post;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		$text = strip_tags($text);
		$excerpt_length = 55;
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words)> $excerpt_length) {
			array_pop($words);
			array_push($words, '<a href="'.get_permalink().'">&#8230;</a>');
			$text = implode(' ', $words);
		}
	}
	return $text;
}

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'improved_trim_excerpt');


/**
 * Cleans image by removing width and height. For responsible design.
 * Using within add_filter
 */
function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
// add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

/**
 * Wraps figure tag around image or image link. 
 * Uses with add_filter() function
 */
function spicy_image($html, $id, $alt, $title, $align, $url, $size ) {
	$html = '<figure>' . $html . '</figure>';
	return $html;
}

add_filter( 'image_send_to_editor', 'spicy_image', 11, 7);


/**
 * Lets clean out our head. Some of them actually very useful.
 */
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
// remove_action( 'wp_head', 'index_rel_link' ); // index link
// remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
// remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
// remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
show_admin_bar(false);

/**
 * Moves admin bar to the bottom.
 */

function move_admin_bar() {
    echo '<style type="text/css">
        .logged-in {
            margin-top: -28px;
        }
        #wpadminbar {
            top: auto !important;
            bottom: 0;
        }
        #wpadminbar .quicklinks .ab-sub-wrapper {
            bottom: 28px;
        }
        #wpadminbar .menupop .ab-sub-wrapper, #wpadminbar .shortlink-input {
            border-width: 1px 1px 0 1px;
            -moz-box-shadow:0 -4px 4px rgba(0,0,0,0.2);
            -webkit-box-shadow:0 -4px 4px rgba(0,0,0,0.2);
            box-shadow:0 -4px 4px rgba(0,0,0,0.2);
        }
        #wpadminbar .quicklinks .menupop ul#wp-admin-bar-wp-logo-default {
            background-color: #eee;
        }
        #wpadminbar .quicklinks .menupop ul#wp-admin-bar-wp-logo-external {
            background-color: white;
        }
        body.wp-admin div#wpwrap div#footer {
            bottom: 28px !important;
        }
    </style>';
}

// add_action('wp_head', 'move_admin_bar');

add_theme_support( 'automatic-feed-links' );
 if ( is_singular() ) wp_enqueue_script( "comment-reply" );

/**
 * This is a modification of image_downsize() function in wp-includes/media.php
 * we will remove all the width and height references, therefore the img tag 
 * will not add width and height attributes to the image sent to the editor.
 * 
 * @param bool false No height and width references.
 * @param int $id Attachment ID for image.
 * @param array|string $size Optional, default is 'medium'. Size of image, either array or string.
 * @return bool|array False on failure, array on success.
 */
function myprefix_image_downsize( $value = false, $id, $size ) {
    if ( !wp_attachment_is_image($id) )
        return false;

    $img_url = wp_get_attachment_url($id);
    $is_intermediate = false;
    $img_url_basename = wp_basename($img_url);

    // try for a new style intermediate size
    if ( $intermediate = image_get_intermediate_size($id, $size) ) {
        $img_url = str_replace($img_url_basename, $intermediate['file'], $img_url);
        $is_intermediate = true;
    }
    elseif ( $size == 'thumbnail' ) {
        // Fall back to the old thumbnail
        if ( ($thumb_file = wp_get_attachment_thumb_file($id)) && $info = getimagesize($thumb_file) ) {
            $img_url = str_replace($img_url_basename, wp_basename($thumb_file), $img_url);
            $is_intermediate = true;
        }
    }

    // We have the actual image size, but might need to further constrain it if content_width is narrower
    if ( $img_url) {
        return array( $img_url, 0, 0, $is_intermediate );
    }
    return false;
}

/* Remove the height and width refernces from the image_downsize function.
 * We have added a new param, so the priority is 1, as always, and the new 
 * params are 3.
 */
// add_filter( 'image_downsize', 'myprefix_image_downsize', 1, 3 );

/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function posted_on() {
	printf( 
		'<a href="%1$s" title="%2$s" rel="bookmark"><time  datetime="%3$s" pubdate>%4$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date('c') ),
		esc_html( get_the_date() )
	);
}

/**
 * Custom formats
 */
add_theme_support( 'post-formats', array('aside', 'gallery', 'image', 'quote', 'video') );

/**
 * Post thumbnails
 */
add_theme_support( 'post-thumbnails' ); 

add_image_size( 'galley-thumbnails', 700, 300, true);
add_image_size( 'test-thumb', 50, 500, true);

?>
