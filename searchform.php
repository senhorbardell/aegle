<?php
/**
 * Form partial
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.0
 */
?>
<form action="<?php echo home_url('/') ?>" method="get" class="searchform">
        <input type="text" name="s" id="search" placeholder="search" value="<?php the_search_query(); ?>" />
</form>