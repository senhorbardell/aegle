<?php 
/**
 * Template for single post.
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.1
 */
get_header(); ?>

<section class="content">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1 class="title">
			<a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
		</h1>

		<h2 class="next-title"><?php next_post_link('%link') ?></h2>

		<h2 class="prev-title"><?php previous_post_link('%link') ?></h2>

		<?php while ( have_posts() ) : the_post() ?>

			<?php the_content() ?>

	</article>

	<?php comments_template() ?>

</section>

		<?php endwhile ?>

<?php get_footer(); ?>