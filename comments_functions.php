<?php
/**
 * Comment form
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.0
 */
function steemy_comments() {

	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );


	$fields =  array(
		'author' => '' .
		            '<p><input id="author" name="author" type="text" placeholder = "Name"' . ( $req ? ' required' : '' ) . $aria_req . '/></p>',
		            
		'email'  => ''.
		            '<p><input id="email" name="email" type="email" placeholder="Email"' . ( $req ? ' required' : '' ) . $aria_req . ' /></p>',
		            
		'url'    => ''.
		            '<p><input id="url" name="url" type="url" placeholder="website" /></p>'

	);
	return $fields;
}

add_filter('comment_form_default_fields', 'steemy_comments');

/**
 * Comment field
 */
function steemy_commentfield() {	

	$commentArea = '<p><textarea id="comment" name="comment" aria-required="true" required ></textarea><p>';
	
	return $commentArea;

}

add_filter('comment_form_field_comment', 'steemy_commentfield');

/**
 * Comment thread
 */
function metro_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>

		<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
			<div id="comment-<?php comment_ID(); ?>">
				<aside>
					<?php echo get_avatar($comment,$default='<path_to_url>' ); ?>
				</aside>

				<div class="author">
					
					<?php $user_name_str = substr(get_comment_author(),0, 20); ?>
					<?php printf('<cite><b>%s</b></cite><span>,</span>', $user_name_str) ?>
					<time><?php printf('%1$s at %2$s', get_comment_date('F j'),  get_comment_time()) ?></time>
					<?php edit_comment_link('(Edit)','  ','') ?>

				</div>

				<?php if ($comment->comment_approved == '0') : ?>

					<em>Your comment is awaiting moderation</em>
					<br />

				<?php endif; ?>

				<div class="comment-text">
					<?php comment_text() ?>
				</div>

				<footer>
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				</footer>
			</div>
<?php
 }