<?php
/**
 * Main template file.
 * 
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.0
 */
?>

<?php get_header() ?>

	<?php if ( have_posts() ) : ?>

		<section class="content">

		<?php while ( have_posts() ) : the_post() ?>
			
				<?php get_template_part( 'content' ) ?>
			
		<?php endwhile ?>

		</section>

	<?php endif ?>

<?php get_footer() ?> 