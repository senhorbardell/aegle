<?php 
/**
 * Template Name: Gallery
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.1
 */
get_header(); ?>

<section class="content">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="title-wrapper"><?php # not semantic! ?>
			<h1 class="title">
				<a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
			</h1>
		</div>


		<?php while ( have_posts() ) : the_post() ?>

			<?php the_content() ?>

	</article>

</section>

		<?php endwhile ?>

<?php get_footer(); ?>