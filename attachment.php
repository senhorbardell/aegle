<?php 
/**
 * Template for displaying attachments.
 *
 * @package Wordpress
 * @subpackage Aegle
 * @since Aegle 1.1
 */
get_header(); ?>

<section class="content" >

	<?php the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1 class="title">
			<a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
		</h1>
		
		<section class="content">

			<?php if ( wp_attachment_is_image() ) : 
				$attachments = array_values( get_children( array( 'post_parent' => $post->post_parent, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID' ) ) );
	foreach ( $attachments as $k => $attachment ) {
		if ( $attachment->ID == $post->ID )
			break;
	}
	$k++;
	// If there is more than 1 image attachment in a gallery
	if ( count( $attachments ) > 1 ) {
		if ( isset( $attachments[ $k ] ) )
			// get the URL of the next image attachment
			$next_attachment_url = get_attachment_link( $attachments[ $k ]->ID );
		else
			// or get the URL of the first image attachment
			$next_attachment_url = get_attachment_link( $attachments[ 0 ]->ID );
	} else {
		// or, if there's only 1 image attachment, get the URL of the image
		$next_attachment_url = wp_get_attachment_url();
	}
		
?>

		<figure class="attachment">
			<a 
				href="<?php echo $next_attachment_url; ?>" 
				title="<?php echo esc_attr( get_the_title() ); ?>" 
				rel="attachment">
					<?php $attachment_size = apply_filters( 'twentyten_attachment_size', 1200 );
						echo wp_get_attachment_image( $post->ID, array( $attachment_size, 9999 ) ); 
						// filterable image width with, essentially, no limit for image height.
					?>
			</a>
		</figure>

		<nav>
			<div class="prev"><?php previous_image_link( false, '&larr; Prev'  ); ?></div>
			<div class="next"><?php next_image_link(false, 'Next &rarr;'); ?></div>
		</nav><!-- #nav-below -->
		
<?php else : ?>
	<a href="<?php echo wp_get_attachment_url(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" rel="attachment"><?php echo basename( get_permalink() ); ?></a>
<?php endif; ?>
<details>
	<summary>
		<?php
		printf( '<span class="%1$s">Published</span> %2$s',
			'meta-prep meta-prep-entry-date',
			sprintf( '<span class="entry-date"><time datetime="%1$s" class="published" >%2$s</time>.</span>',
				esc_attr( get_the_date('c') ),
				get_the_date()
			)
		); ?>

<?php	if ( wp_attachment_is_image() ) {
			$metadata = wp_get_attachment_metadata();
			printf( 'Full size is %s pixels',
				sprintf( '<a href="%1$s" title="%2$s">%3$s &times; %4$s</a>',
					wp_get_attachment_url(),
					esc_attr( 'Link to full-size image' ),
					$metadata['width'],
					$metadata['height']
				)
			);
		}
		?>
	</summary>
			<?php the_content() ?>
</details>
		</section>

	</article>

	<?php comments_template() ?>

</section>

	<?php wp_link_pages(); ?>

<?php get_footer(); ?>