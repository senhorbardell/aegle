<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.1
 */

get_header(); ?>

<section class="content" role="main">

	<article id="post-0" class="post error404 no-results not-found">

			<h1 class="title">Not found</h1>
			<p>
				Seems like we can't find what you looking for. Try searching again.
			</p>

	</article>

</section>

<?php get_footer(); ?>