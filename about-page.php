<?php 
/**
 * Template Name: About page
 *
 * @package WordPress
 * @subpackage Aegle
 * @since Aegle 1.1
 */

/**
 * Data processing
 */
if(isset($_POST['submitted'])) {
	$name = trim($_POST['p-name']);
	$email = trim($_POST['p-email']);
	if(function_exists('stripslashes')) $comment = stripslashes(trim($_POST['p-comment'])); 
	else $comment = trim($_POST['p-comment']);
	
	$emailTo = get_option('admin_email');
	$subject = $name.' want to contact you.';
	$body = 'Name: $name \n Email: $email \n Comment: $comment';
	$headers = 'From: '.$name.' <'.$emailTo.'>'.'\r\n'.'Reply-To: '.$email;
	
	if (wp_mail($emailTo, $subject, $body, $headers)) $done = true;
	else $done=false;
}

get_header(); ?>

<section <?php body_class('content') ?>>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1 class="title">
			<a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
		</h1>
		
		<?php the_post(); ?>

		<section class="content">
			<?php the_content() ?>

			<form action="" id="contact-form" method="post">
			<p>
				<input type="text" name="p-name" placeholder="Name" required>
			</p>
			<p>
				<input type="email" name="p-email" placeholder="Email" required>
			</p>
			<p>
				<input type="url" name="p-website" placeholder="website">
			</p>
			<p>
				<textarea name="p-comment" required aria-required="true"></textarea>
			</p>
			<p>
				<input type="submit" value="Send" name="p-submit">
			</p>
			
			
			<input type="hidden" name="submitted" value="true">
			<input type="hidden" name="link" value="<?php echo get_permalink(); ?>">
			</form>

		<?php if(isset($_GET['status'])) echo 'Thank you for your message.'; ?>

		</section>

	</article>

</section>

<?php get_footer(); ?>